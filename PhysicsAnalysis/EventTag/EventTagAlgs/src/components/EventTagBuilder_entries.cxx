#include "../RawInfoSummaryTagBuilder.h"
#include "../GlobalEventTagBuilder.h"
#include "../GlobalTriggerTagBuilder.h"

#include "EventTagAlgs/EventSplitter.h"


DECLARE_COMPONENT( RawInfoSummaryTagBuilder )
DECLARE_COMPONENT( GlobalEventTagBuilder )
DECLARE_COMPONENT( GlobalTriggerTagBuilder )

DECLARE_COMPONENT( EventSplitter )
